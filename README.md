# Campus Academy - Contest 2020
## Christmas Tree

### Algorithme Bonus 1 :
* Le tron du sapin est mon stylé. Mais il est comme l'exemble ..
```JS
Variables resultat EN CHAINE_DE_CARACTERE

DEBUT

FONCTION afficherEtoiles (nombre)
  POUR i ALLANT DE 1 A nombre
    resultat <- resultat + '*'
  FIN POUR
  
  RETOURNE resultat
FIN FONCTION

FONCTION afficherCentre (text, alignement)
  resultat = ""
  POUR i ALLANT DE 1 A alignement
    SI i < (taille(text)/2)
      resultat += " "
    SINON SI
      resultat += souschaine(text, i, i+1)
    FIN SI
  FIN POUR
  
  AFFICHER resultat
FIN FONCTION

FONCTION getBase (floor)
  RETOURNE ((floor-1)*2)+1
FIN FONCTION

FONCTION getMax (floor)
  RETOURNE getBase(floor) + ((floor * 2) * 3)
FIN FONCTION

FONCTION getBottomChar(col, row)
  result <- ""
  SI row < 4 ET row > 8
    SI col == 0
      result <- "|"
    SINON SI col == 1
      result <- "0"
  SINON
    RETOURNER "*"
  FIN FIN
  
  SI row < 4
    RETOURNER result+" "
  SINON SI row > 8
    RETOURNER " "+result
  FIN SI
FIN FONCTION

FONCTION printBottom(alignement)
  POUR col ALLANT DE 0 A 3
      result <- ""
      POUR row ALLANT DE 0 A 5
        result += getBottomChar(col, row)
      FIN POUR
      AFFICHER afficherCentre(resultat, alignement)
  FIN POUR
FIN FONCTION

FONCTION dessinerTriangle ( incrementation, base, alignement)
  POUR i ALLANT DE 0 A 3
    afficherCentre ( afficherEtoiles((i*incrementation) + base) )
  FIN POUR
FIN FONCTION

FONTION dessinerSapin ( height )
  POUR i ALLANT DE 1 A ( height + 1 )
    dessinerTriangle(i*2, getBase(i), getMax(i))
  FIN POUR
  
  printBottom()
FIN FONCTION

dessinerSapin(3)

FIN
```
