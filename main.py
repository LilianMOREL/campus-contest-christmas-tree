def getStarsOnBase(triangle: int) -> int:
    return ((triangle-1)*2)+1

def getMaxRowStars(triangle :int) -> int:
    return getStarsOnBase(triangle) + ((triangle * 2) * 3)

def getBottomChar(col :int, row :int, length :int) -> str:
    result = ""
    startWood = (length-1)*2
    endWood = startWood + 6

    if row <= startWood or row >= endWood:
        if col == 0:
            result = "|"
        elif col == 1:
            result = "0"
    else:
        return "*"

    if row <= startWood:
        return result+" "
    elif row >= endWood:
        return " " + result

def printBottom(length :int, align :int):
    for col in range(0, 3):
        result = ""
        for row in range(1, (2*((length-1)*2))+6):
            result += getBottomChar(col, row, length)
        print(result.center(align))

def printTriangle(incr :int, base :int, align :int, triangleNumber :int):
    for i in range(4):
        starsNumber = base + (i*incr)
        print( addBubble((starsNumber * "*"), i, triangleNumber).center(align))

def addBubble(text :str, col :int, triangleNumber :int) -> str:
    if(col == 0 and triangleNumber != 1):
        space = ""
        for i in range(int((getMaxRowStars(triangleNumber) - len(text))/2)-5):
            space += " "

        return "0"+space+text+space+"0"
    else:
        return text

def printStar(align :int):
    for v in range(-3, 4):
        result = ""
        for h in range(1, 8):
            if v == 0 or (abs(v) >= 1 and h == 4) or ((abs(v) == 2) and (abs(h) == 2 or abs(h) == 6)) or (abs(v) == 3 and (abs(h) == 1 or abs(h) == 7)):
                if v > 1 and h == 4:
                    result += "|"
                elif v == 0 and h == 4:
                    result += " "
                else:
                    result += "*"
            else:
                result += " "
            result += " "
        print(result.center(align))


def printSapin(height :int):
    align = getMaxRowStars(height)

    if(height > 1):
        printStar(align)

    for i in range(1, height+1):
        printTriangle(i*2, getStarsOnBase(i), align, i)

    printBottom(height, align)

size = input("Give me your number")
printSapin(int(size))